# Ansible Role: Docker

This role installs Docker on the host, no variables need to be set.
The role has been tested on CentOS 8, Ubuntu 22, Debian 12.

## Requirements

None.
