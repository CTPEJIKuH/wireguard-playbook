Role Name
=========
WireGuard - wg-easy

Description
-----------
Installs wg-easy from `https://github.com/wg-easy/wg-easy`.

Requirements
------------
Ensure the following variables are set:
- `wg_easy_password_hash`: you need create a password hash `https://github.com/wg-easy/wg-easy/blob/master/How_to_generate_an_bcrypt_hash.md`

The directory where docker-compose will be copied is specified by the variable:
- `wg_workdir`: "/data/wireguard"

The role backs up the wireguard folder. The folder contains:
- docker-compose.yml
- wg0.conf
- wg0.json which stores the list of users.

Activate/Deactive backup 
- `wg_backup`: "True"

The directory where backups will be stored is specified by the variable:
- `wg_backupdir`: "/data/backup/wireguard"

Backup scheduling can be configured with the following variables:
- `wg_backup_minute`: "0"
- `wg_backup_hour`: "0"
- `wg_backup_day`: "0"
- `wg_backup_month`: "0"
- `wg_backup_weekday`: "0"
By default, backups are scheduled to run daily at 00:00.
Additionally, the script deletes backup files older than 14 days.

Image and container name can be set via variables:
- `wg_image`: "ghcr.io/wg-easy/wg-easy"
- `wg_container_name`: "wg-easy"

Variable defining the port for WireGuard operation:
- `wg_port`: "51820"

Variables defining ports for the WebUI:
- `wg_ui_port`: "51821"

Restart policy on failure:
- `wg_container_restart`: "unless-stopped"

Ethernet network device through which WireGuard traffic should pass:
- `wg_device`: "eth0"

Value in seconds to keep the "connection" open. If this value is 0, connections will not be kept alive:
- `wg_persistent_keepalive`: "0"

UI language:
- `wg_lang`: "en"

Display more detailed traffic statistics in the UI:
- `wg_ui_traffic_stats`: "false"

Sets the Wireguard address:
- `wg_default_address`: "10.8.0.1"

Sets DNS for Wireguard:
- `wg_default_dns`: "1.1.1.1"

MTU values:
- `wg_mtu`: "1420"

Sets the allowed IPs for WireGuard connections:
- `wg_allowed_ips`: "192.168.15.0/24, 10.0.1.0/24"

Specifies commands to execute before bringing up the WireGuard interface:
 - `wg_pre_up`: "echo 'Pre Up' > /etc/wireguard/pre-up.txt"

Specifies commands to execute after bringing up the WireGuard interface: 
- `wg_post_up`: "echo 'Post Up' > /etc/wireguard/post-up.txt"

Specifies commands to execute before bringing down the WireGuard interface: 
- `wg_pre_down`: "echo 'Pre Down' > /etc/wireguard/pre-down.txt"

Specifies commands to execute after bringing down the WireGuard interface:
- `wg_post_down`: "echo 'Post Down' > /etc/wireguard/post-down.txt"