- name: Create directories
  ansible.builtin.file:
    path: "{{ item.path }}"
    state: directory
    mode: "{{ item.mode }}"
    recurse: true
    owner: "{{ item.owner }}"
    group: "{{ item.group }}"
  loop:
    - { path: "{{ wg_workdir }}", mode: "0755", owner: "root", group: "root" }

- name: Copy compose files
  ansible.builtin.template:
    src: "{{ item.j2 }}"
    dest: "{{ item.target }}"
    mode: "0755"
    owner: "root"
    group: "root"
  loop:
    - { j2: 'docker-compose.yml.j2', target: "{{ wg_workdir }}/docker-compose.yml" }
    - { j2: 'wireguard-docker.service.j2', target: "/etc/systemd/system/wireguard-docker.service" }
  notify:
    - "Restart wireguard"

- name: Start containers via systemd
  ansible.builtin.systemd:
    name: "wireguard-docker.service"
    state: started
    enabled: true
    daemon_reload: true
  when: not ansible_check_mode

- name: Create directories for backup
  ansible.builtin.file:
    path: "{{ item.path }}"
    state: directory
    mode: "{{ item.mode }}"
    recurse: true
    owner: "{{ item.owner }}"
    group: "{{ item.group }}"
  loop:
    - { path: "{{ wg_backupdir }}", mode: "0755", owner: "root", group: "root" }
  when: wg_backup is defined and wg_backup == "True"

- name: Copy compose files for backup
  ansible.builtin.template:
    src: "{{ item.j2 }}"
    dest: "{{ item.target }}"
    mode: "0755"
    owner: "root"
    group: "root"
  loop:
    - { j2: 'backup.sh.j2', target: "{{ wg_backupdir }}/backup.sh" }
  notify:
    - "Restart wireguard"
  when: wg_backup is defined and wg_backup == "True"

- name: Schedule cron job for backup script
  ansible.builtin.cron:
    name: "Run WireGuard backup script daily"
    minute: "{{ wg_backup_minute }}"
    hour: "{{ wg_backup_hour }}"
    day: "{{ wg_backup_day }}"
    month: "{{ wg_backup_month }}"
    weekday: "{{ wg_backup_weekday }}"
    job: "/bin/bash {{ wg_backupdir }}/backup.sh"
    state: present
    user: root
  when: wg_backup is defined and wg_backup == "True"

- name: Output UI address
  ansible.builtin.debug:
    msg: "UI address: http://{{ ansible_host }}:{{ wg_ui_port }}"
