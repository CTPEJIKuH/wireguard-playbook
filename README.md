Description
--------
To install WireGuard Easy(`https://github.com/wg-easy/wg-easy`), you need to set a variable in inventories/prod/group_vars/all.yml: 
`wg_easy_password_hash`, which will set the password for the wg-easy web interface.
You need create a password hash `https://github.com/wg-easy/wg-easy/blob/master/How_to_generate_an_bcrypt_hash.md`

Additionally, set the VM name and IP address in inventories/prod/inventory.yml.

Entry Point
-----------
At the end of the Ansible run, a message like the following will be displayed:
`UI address: http://123.45.67.890:51821`
This will be the web interface.

Backup
-----------
Backups of the entire WireGuard folder will be created daily and stored in /data/backup/wireguard, deleting files older than 14 days.

Activate/Deactive backup 
- `wg_backup`: "True"

Example Playbook
----------------
Deployment with a check:
`ansible-playbook -i inventories/prod main.yml -D -C`
